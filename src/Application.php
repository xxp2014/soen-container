<?php


namespace Soen\Container;


use Soen\Http\Server\Handler;

class Application
{
    public $context;
    public $configArray;
    function __construct($path, $configArray)
    {
        $this->configArray = $configArray;
        $this->context = new ApplicationContext(new Container($path));
        \App::$app = $this;
    }
    
    function run(){
	    // 加载所有 命令行
        context()->getComponent('command');
    }
}